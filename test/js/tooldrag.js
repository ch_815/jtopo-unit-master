//拖拽节点的初始化，以及拖拽拖拽操作
 
$(document).ready(function(){
    init("server_icon");    //云主机
    init("exchanger_icon"); //交换机
});

function init(id){
   var canvasSrc = document.getElementById(id);
   var contextSrc = canvasSrc.getContext('2d');
   var image = new Image();
    image.src="img/"+id+".png";
    image.οnlοad=function(){
            contextSrc.drawImage(image,0,0);
    }
}



function allowDrop(ev){
       ev.preventDefault();
}

//拖拽开始
function drag(ev){
       ev.dataTransfer.setData("Text", ev.target.id);  
}

//拖拽结束
function drop(ev){
       //ev.preventDefault();
      var data = ev.dataTransfer.getData("Text");
       //ev.target.appendChild(document.getElementById(data));
       draw(ev,data);
}